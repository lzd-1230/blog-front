import { createApp } from 'vue'
import App from './App.vue'
import router from './router/index.js'

// md插件
import VMdPreview from '@kangc/v-md-editor/lib/preview';
import '@kangc/v-md-editor/lib/style/preview.css';
import VMdEditor from '@kangc/v-md-editor';
import '@kangc/v-md-editor/lib/style/base-editor.css';
import githubTheme from '@kangc/v-md-editor/lib/theme/github.js';
import '@kangc/v-md-editor/lib/theme/style/github.css';
import hljs from 'highlight.js';

//elementui
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'

//全局样式文件
import "./assets/css/reset.scss"

//axios
import axios from "axios"
axios.defaults.withCredentials = true;


//vue-cookies插件
import VueCookies from 'vue-cookies'


VMdPreview.use(githubTheme, {
    Hljs: hljs,
});

VMdEditor.use(githubTheme, {
  Hljs: hljs,
});


const app = createApp(App);
app.use(router);
app.use(VMdPreview);
app.use(VMdEditor);
app.use(ElementPlus);
app.use(VueCookies)
app.mount('#app')