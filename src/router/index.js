//路由配置文件
import { createRouter, createWebHistory } from "vue-router";
import routes  from "./router.config.js"

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),  //历史模式
    routes
})

export default router;