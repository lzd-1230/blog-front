const Index = () => import("../views/Index.vue");
const Article = () => import("../views/Article.vue");
const Auth = () => import("../views/Auth.vue");
const SelfCenter = () => import("../views/SelfCenter.vue");
const ArticleRelease = () => import("../views/ArticleRelease.vue")

const routes = [
    {
        path: "/",
        component: Index
    },
    {
        path: "/article/:id",
        component: Article
    },
    {
        path: "/login/",
        component: Auth
    },
    {
        path: "/self_center/",
        component: SelfCenter
    },
    {
        path: "/article_release/",
        component: ArticleRelease
    }
]

export default routes;