import {defineConfig} from "vite";
import path from "path";
import vue from '@vitejs/plugin-vue'


export default defineConfig({
    plugins: [vue()],
    envPrefix: "ENV_",
    //路径解析
    resolve: {
        alias: {
            "@": path.resolve(__dirname, "./src"),
        }
    },
    //全局加入css样式
    cssPreprocessOptions: {
        scss: {
        additionalData: '@import "./src/assets/css/reset.scss";' // 添加公共样式
        }
    }
})