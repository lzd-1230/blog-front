import { defineConfig, loadEnv, resolveEnvPrefix } from 'vite'
import {resolve} from "path";
import viteBaseConfig from "./vite.config.base.js"
import viteDevConfig from "./vite.config.dev.js"
import viteProConfig from "./vite.config.pro.js"


const envResolver = {
  "build": () => {
      return {...viteBaseConfig, ...viteProConfig}
  },
  "serve": () => {
      return {...viteBaseConfig, ...viteDevConfig}
  }
}

//根据不同的npm命令选择不同的配置文件
export default defineConfig(({command, mode}) => {
  const env = loadEnv(mode, process.cwd(), "");  //这里配置文件就会根据mode找对应的.env.{mdoe}文件
  return envResolver[command]();
})  
